// Declare Vars
var doc         = document,
    form        = doc.querySelector('form'),
    tbody       = doc.querySelector('tbody'),
    divMsg      = doc.getElementById('messages'),
    cItems      = 0;
  
  function init() {

  /*var data = getData(
    
    displayBsn, 
    function() { 
      userMessage('', 'notGet')
    }
  );*/
    
  // Setting new business event
  
  setEvent(newBusiness, "click", liteboxActivate, ['.litebox', '.litebox-form'])
  }

  function displayBsn(bsn) {

    if(typeof bsn === 'object') {
      cItems = Object.keys(bsn).length;
    }

    tbody.innerHTML = '';

    if(cItems === 0) {
      
      var tr;
      tr = document.createElement('tr');

      var td;
      td = td.createElement('td');
      td.setAttribute('colspan', '3'),
      td.innerText = 'No Businesses Set';
      tr.appendChild(td);
      tbody.appendChild(tr);
    }
    else {
      var tr = []

      for (var key in bsn) {
        
        if(!bsn.hasOwnProperty(key)) {
          continue;
        }

          tr = doc.createElement('tr');
          tr.setAttribute('data-pos', bsn[key]['position']);
          
          var td = document.createElement('td');
          td.innerText = key;
          
          tr.appendChild(td);
          
          td = document.createElement('td');
          td.className = 'color';
          td.setAttribute('data-color', bsn[key]['color']);
          var span = document.createElement('span');
          span.style.backgroundColor = bsn[key]['color'];
          td.appendChild(span);
          tr.appendChild(td);

          td = document.createElement('td');
          td.setAttribute('colspan', 2)
          var button = document.createElement('button');
          button.setAttribute('class', 'delete');
          var i = document.createElement('i');
          i.setAttribute('class', 'fa fa-trash');
          button.appendChild(i);
          td.appendChild(button);
          tr.appendChild(td);
          tbody.appendChild(tr);
          
      }
    }

    var deleteClass = tbody.querySelectorAll('.delete'),
        fn = deleteItem,
        
        args = {
          
          successCb : function(name) {

            userMessage(name, 'delSuccess', false);
            var data = getData(displayBsn, function() { userMessage('', 'notGet')}); 
          },
          errorCb : function() {
            userMessage(name, 'delError');
          },

        }
        setEvent(deleteClass, 'click', fn, args); 
        
  }

  function liteboxActivate($this, elements) {
    
    var el = elements.length;
    var i = 0;

    for(i; i < el; i++) {
      elements[i].style = "display:block";
    }
  }

function validate() {
    
    var name  = form.querySelector('.business').value,
        color = form.querySelector('.bgColor').value.toUpperCase(),
        pos   = form.querySelector('.position').value;
        
    if(isEmpty(name)) {
      userMessage('Business', 'isEmpty');
      return;
    }
    if(isValid(name, 'business')) {
      userMessage('Business', 'isValid');
      return;
    }
    if(isEmpty(color)) {     
      userMessage('Color', 'isEmpty');
      return;
    }
    if(isValid(color, 'bgColor')) {
      userMessage('Color', 'isValid');
      return;
    }
    if(typeof pos === 'string') {
      pos = parseInt(cItems + 1);
    }
    if(!Number.isInteger(pos)) {
      userMessage('position', 'notNumber');
      return;
    }
    
    var set = {};
    set[name] = {color: color, type : 1, position : pos};
    
    setData(set, name,
      function() {
        userMessage(name, 'success', false);
      },
      function() {
        userMessage('', 'notSet');
    });

    var data = getData(displayBsn, function() { userMessage('', 'notGet')}); 
    
  }

function userMessage(value, message, hasError = true) {
    
    var cssClass = "messages-error";
    
    if(hasError === false) {
      cssClass = "messages-success";
    }

  var userMsg     = {
      isEmpty     : "{name} field cannot be empty.",
      isValid     : "{name} is not in a valid format.",
      isUpdated   : "{name} has been updated successfully.",
      notNumber   : "{name} is not a number",
      notGet      : "There was a problem getting the businesses from local storage",
      notSet      : "There was a problem storing {name} in the storage",
      success     : "{name} background color has been saved.",
      delSuccess  : "{name} was deleted successfully.",
      delError    : "{name} was not deleted"
  };

  var pMsg = '<p class="' + cssClass + '">' +  userMsg[message].replace(/\{name\}/, value) + '<a href class="fa fa-close"></a></p>';

  divMsg.innerHTML = pMsg;

}

init();