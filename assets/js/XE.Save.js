"use strict";

var XE = XE || {}
// Save data for use later
XE.Save = {

  message : '',
  error : false,
  inputs : {
    bannerPosition : '1'
  },

  Reset : function(resets) {

    for(var key in resets) {
      if(!resets.hasOwnProperty(key)) {
        continue;
      }

      this[key] = resets[key]; 
    }

  }
}