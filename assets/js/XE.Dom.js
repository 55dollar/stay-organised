"use strict";

var XE = XE || {};

XE.Dom = {

  d : document,

  ElementBuilder : function(element, props) {

    var el = this.d.createElement(element);
    
    if(typeof props === 'undefined') {
      return el;
    }

    for(var key in props) {
      
      if(!props.hasOwnProperty(key)) {
        continue;
      }

      this._propCreator(key, props[key], el);
    }    
    return el;
  },

  _propCreator : function(type, prop, el) {
    var addProps
    switch (type) {
      case "class":
        return el.className += " "  + prop;
      case "text":
        return el.textContent = prop;
      case "attr":
        var props = prop.split("=");
        return el.setAttribute(props[0], props[1]);
      case "style":
        return el.style.cssText = prop;
        break;
      case "colSpan":
        return el.colSpan = prop;
      case "default":
        return el;
    }

  },

  AddReplaceChild : function(parent, el) {

    var sel = XE.Cf.doc.querySelector(parent)
    sel.innerHTML = "";
    sel.appendChild(el);
  },

  HideElement : function(el) {

    if(typeof el === "string") {
      el = XE.Cf.doc.querySelector(el);
    }
    el.style.display = "none";
  }
}