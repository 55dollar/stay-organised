"use strict";
// XeroError object literal notation
var XE = XE || {};


XE.UI = {
  // Litebox for form in this application
  LiteBox : function(options) {

    var button = XE.Cf.doc.querySelector(options.button),
        lBox = XE.Cf.doc.querySelector(options.lBox),
        lBoxChild = lBox.children[0];
  
    var events = function() {

     
      button.addEventListener('click', function(e) {
        
        e.preventDefault();
        lBox.style.display = "block";
      });
      lBox.addEventListener('click', function(e) {
        lBox.style.display = "none";
      });
      // Stop Litebox from closing if LiteBox is closed
      lBoxChild.addEventListener('click', function(e) {
        e.stopPropagation();
      })
    }
  
  events();

  },
  // AddBannerPos takes 
  AddBannerPos : function(options) {
    
    var form = XE.Cf.doc.querySelector(options.form),
        ul = form.querySelector(options.ul),
        li = ul.querySelectorAll(options.li);

    var events = function() {

      var liLength  = li.length, i = 0;

      for(i; i<liLength; i++) {
        
        li[i].addEventListener('click', function(e) {
            
            XE.Lib.Helper.RemoveClass(li, options.class);
            this.classList.add(options.class); 
            XE.Cf.doc.querySelector('[name=' + options.name + ']').value = this.getAttribute(options.attr);
        });
      }
    }
    events(); 
  },

  OnFormSubmit : function(options) {

    var form = XE.Cf.doc.querySelector(options.form),
        save = form.querySelector(options.save),
        cancel = form.querySelector(options.cancel);

    var events = function() {

      save.addEventListener('click', function(e) {
        
        e.preventDefault();

        var v = options.validate; 
        for(var key in v) {
          
          if(!v.hasOwnProperty(key)) {
            continue;
          }
          
          // Get inputs and store them in XE.Save.inputs
          var val = XE.Lib.Helper.GetInputValue(key);
          
          // Validating data for mistakes or malicious
          XE.Lib.Validator(v[key], val, key);

        }
        
        if(XE.Save.error === false) {
          
          var name = XE.Save.inputs[XE.Cf.onFormSubmit.name],
              args = []
              args['name'] = name;
          
          var cb = {   
            success : function() {
              XE.Dom.HideElement('.litebox');
              XE.UI.DisplayBusinesses();
            },
            error : function() {
              XE.Misc.AddtoMessages('SetData', args);
              XE.Misc.SetUpMessages();}
            }
          // Declaring objects and saving data
          var data = {};
          data[name] = XE.Save.inputs;     
          XE.Vendor.GApi.SetData(data, cb);

        }
        else {
          XE.Misc.SetUpMessages();
        } 
      });
    }

    events();
  },

  DisplayBusinesses : function(obj = null) {

    if(obj === null) {
      var cb = {
        error : function() { alert('there was a problem, getting businesses from the database')},
        success : function(data) { XE.UI.DisplayBusinesses(data); }
      }
      XE.Vendor.GApi.GetData(cb);
      return;
    }
    
    var size = Object.keys(obj).length;
   
    var tbody = document.querySelector('tbody')

    tbody.innerHTML = ''

    var domEl;

    if(size === 0) {
      
      var tr = XE.Dom.ElementBuilder('tr');
      var props = {colSpan: "3"}
      var td = XE.Dom.ElementBuilder('td', props);

      props = {style : 'color:#333;',text : 'No Businesses Set'}
      var p = XE.Dom.ElementBuilder('p', props);
      tr.appendChild(td).appendChild(p);
      tbody.appendChild(tr);
      return;
    }

    for(var key in obj) {
      
      if(!obj.hasOwnProperty(key)) {
        continue;
      }
      
      var tr = XE.Dom.ElementBuilder('tr'),
          td = XE.Dom.ElementBuilder('td'),
          previewStyle = 'background-color:' + obj[key]['backgroundColor'] + '; border-radius:4px; color:' + obj[key]['textColor'] + ';',
          props = {style : previewStyle, text : key},
          p = XE.Dom.ElementBuilder('p', props);
          tr.appendChild(td).appendChild(p);
      var classes = "pos p" + obj[key]['bannerPosition'];

          props = { class : classes };
      var i = XE.Dom.ElementBuilder('i', props);     
          td = XE.Dom.ElementBuilder('td');
          tr.appendChild(td).appendChild(i);
          
          props = { class : 'delete', attr : 'data-name=' + key}
          td = XE.Dom.ElementBuilder('td');
      var a = XE.Dom.ElementBuilder('a', props);  
          tr.appendChild(td).appendChild(a)
          tbody.appendChild(tr);

    }

    var events = function() {
      console.log('event');
      var dlt = tbody.querySelectorAll('.delete');
      
      var dltL = dlt.length;
      var i = 0;

      for(i; i < dltL; i++) {
       
       dlt[i].addEventListener('click', function(e) {
        
        e.preventDefault();
        var name = this.getAttribute('data-name');
         var cb = {
          error : function() { alert('there was a problem, deleting ' + name + ' from the database')},
          success : function() { XE.UI.DisplayBusinesses(); }
        }
        
        XE.Vendor.GApi.DeleteData(name, cb);
        });
      }
      
    }
    events();
  }
}

XE.UI.LiteBox(XE.Cf.liteBox);
XE.UI.AddBannerPos(XE.Cf.addBannerPos);
XE.UI.OnFormSubmit(XE.Cf.onFormSubmit);
XE.UI.DisplayBusinesses();