"use strict";

XEROERRORS = {


  // LiteBox module displays and hides litebox
  LiteBox : function(options) {

    var newB = this.doc.querySelector(options.newB),
        lBox = this.doc.querySelector(options.lBox),
        lBoxChild = lBox.children[0];
  
    var events = function() {

      newB.addEventListener('click', function(e) {
        e.preventDefault();
        lBox.style.display = "block";
      });

      lBoxChild.addEventListener('click', function(e) {
        e.stopPropagation();
      });

      lBox.addEventListener('click', function(e) {
      
        lBox.style.display = "none";
      });
    }
  
    events();
  },

  AddBusForm : function(options) {
  
    var bForm   = this.doc.querySelector(options.form),
        ul      = bForm.querySelector(options.ul),
        li      = ul.querySelectorAll(options.li);

    var events  = function() {

      // 
      var liLength  = li.length,
          i         = 0;

      for(i; i<liLength; i++) {
        
        li[i].addEventListener('click', function(e) {
         
          doc.RemoveClass(li, options.class);
          this.classList.add(options.class);

        });
      };


    }
    events();
  },

  
}(XEROERRORS = XEROERRORS || {});




  
var addBusForm = {
    options : {
      form    : '.litebox-form',
      ul      : 'ul',
      li      : 'li',
      class   : 'active'
    }
  };

XeroErrors.AddBusForm(addBusForm.options);


console.log(XeroErrors);

(XEROERRORS = {
  
  storage : chrome.storage.sync,

  getData : function(successCb, errorCb = false) {
  
    // Get items from storage
    this.storage.get(function(items) {
    
      if(chrome.runtime.lastError !== undefined) {
        if(typeof errorCb === 'function') {
          errorCb();
        }
        return;
      }
      return(items);

      if(typeof successCb === 'function') {
        successCb(items);
      }
    });
  },

  