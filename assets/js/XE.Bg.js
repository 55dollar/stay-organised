"use strict";
// XeroError object literal notation
var XE = XE || {};

XE.Bg = {

  bannerPosition : {
      1 : "top:0; min-width:100%;",
      2 : "bottom:0; min-width:100%;",
      3 : "top:0; left:0;",
      4 : "top:0; right:0;",
      5 : "bottom:0; left:0;",
      6 : "bottom:0; right:0;"
  },

  CheckBsn : function(items) {
    
    var bsnName = document.querySelector('.org-name a');

    if(bsnName === null) {
      return;
    }
    var bsnName = bsnName.innerText.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    var reg = new RegExp(bsnName, 'i');

    for (var key in items) {
          
      if(!items.hasOwnProperty(key)) {
        continue;
      }

      if(reg.test(key) === false) {
        continue;
      }
      this.CreateBanner(items[key]);
    }
  },

  CreateBanner : function(items) {

    var banner = this.bannerPosition[items['bannerPosition']],
        textColor = items['textColor'],
        bgColor = items['backgroundColor'],
        name = items['businessName'],
        styles = 'position:fixed; font:32px Arial, San-Serif; padding:20px 40px; ' 
                + banner 
                + ' color:' 
                + textColor 
                + '; background-color:' 
                + bgColor + ';'; 
    

    var el = {
        style : styles,
        text : name
    }

    var newEl = XE.Dom.ElementBuilder('div', el);

    document.body.prepend(newEl);
    
  }
}

var cb = {
  error : function() {},
  success : function(items) { XE.Bg.CheckBsn(items); }
}

XE.Vendor.GApi.GetData(cb);

