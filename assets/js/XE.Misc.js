"use strict";
// XeroError object literal notation
var XE = XE || {};

XE.Misc = {

  // Adds Messages to and save
  AddtoMessages : function(userMessage, args) {
    
    XE.Save.error = true;

    var help = XE.Lib.Helper,
        repName = help.SpaceBeforeCaps(help.CapitalFirst(args['name'])),
        message = XE.Cf.message[userMessage],
        // Simple reg to save reg
        regex = /\$/,
        rep = [repName],
        rep = rep.concat(args),
        repL = rep.length;
    
    for(var i = 0; i < repL; i++) {
      
      message = message.replace(regex, rep[i]);
    }
    XE.Save.message += message;   
    
  },

  SetUpMessages : function() {

    var obj = {
        class : 'error',
        text : XE.Save.message
    }

    var el = XE.Dom.ElementBuilder('p', obj);
    XE.Dom.AddReplaceChild('.messages', el);
    XE.Save.Reset({'error' : false, 'message' : ''});
  } 
}