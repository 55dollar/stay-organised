"use strict";
// XeroError object literal notation
var XE = XE || {};

XE.Cf = {

  doc : document,
  form : document.querySelector('form'),

  liteBox : {
    button : '.new',
    lBox : '.litebox'
  },

  addBannerPos : {
    form : '.litebox-form',
    ul : 'ul',
    li : 'li',
    class : 'active',
    attr : 'data-pos',
    name: 'bannerPosition'
  },

  onFormSubmit : {
    form : '.litebox-form',
    save : '.save',
    cancel : '.cancel',
    name : 'businessName',
    validate : {
      businessName : {
          NumRange : [1,64],
          RegexValid : /^([a-zA-Z0-9 \.\'\~\?\!\(\)]){1,64}$/
      },
      backgroundColor : {
           RegexValid : /^\#([0-9a-fA-F]){6}$/
      },
      textColor : {
          RegexValid : /^\#([0-9a-fA-F]){6}/
      },
      bannerPosition : {
          NumRange : [1,6]
      }
    }
  },

  message : { 
    RegexValid : "The $ field is not in the correct format.",
    NotEmpty : "The $ field cannot be empty.",      
    IsNumeric : "The $ field needs to be numerical.",
    NumRange: "The $ field must contain a min of $ and a max $ characters.",
    GetData : "There was a problem getting your data from local storage",
    DeleteData : "There was a problem deleting your data.",
    SetData : "There was a problem storing your data."
  }
}