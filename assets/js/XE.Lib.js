"use strict";

var XE = XE || {}

XE.Lib = {
  
  Helper : {

    RemoveClass : function(myEl, cl) {
      
      if(typeof myEl === "string") {
        
        var el = XE.Cf.doc.querySelector('.' + myEl);
        el.classList.remove(cl);
        return;
      }

      myEl.forEach(function(el) {
              
        if (el.classList.contains(cl)) {
          el.classList.remove(cl);
        }
      });
    },

    GetInputValue : function(el) {
      
      var input = XE.Cf.doc.querySelector('[name=' + el + ']').value;
    
      XE.Save.inputs[el] = input; 
      
      return input;
    },

    ReturnStr : function(key, value) {
        // Filtering out properties
        if (typeof value === 'string') {
          return undefined;
        }
        return value;
    },

    CapitalFirst : function(name) {
    return name.charAt(0).toUpperCase() + name.slice(1);
    },

    SpaceBeforeCaps : function(name) {
    return name.replace(/([A-Z])/g, ' $1').trim();
    }
  },
  // functions that validate users input
  Validator : function(val, inputValue, name) {

    // Set vars
    var valid = true;

    // loop the input name value
    for(var key in val) {
      
      if(!val.hasOwnProperty(key)) {
        continue;
      }

      // Checking the vailidators bolow to see if user input is valid
      valid = this[key](inputValue, val[key]),
      
      val[key]['name'] = name;
      
      if(valid === true) {
        continue;
      }
      
      XE.Misc.AddtoMessages(key, val[key]);
    }
  },

  RegexValid : function(value, args) {
    var regX = args

    if(value.match(regX) !== null) {
      return true;
    }
      return false;
  },

  NotEmpty : function(value) {

    if(value.length === 0) {
      return false;
    }
    return true;
  },

  IsNumeric: function(value ) {

    if(typeof value !== "number") {
      return false;
    }
    return true;
  },

  NumRange: function(value, args) {
  
    var min = parseInt(args[0]),
        max = parseInt(args[1]);
        value = parseInt(value);

    if((value < min) || (value > max)) {
      return false;
    }
      return true;

  }
}