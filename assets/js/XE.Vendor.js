"use strict";
// XeroError object literal notation
var XE = XE || {};

XE.Vendor = {

  // Google storage extensions api 
  GApi : {
    
    storage : chrome.storage.sync,

    GetData : function(cb) {
      
      // Get items from storage
      this.storage.get(function(items) {

          XE.Vendor.GApi.CheckError(cb, items);
      });
    },

    SetData : function(data, cb) {

      this.storage.set(data, function() {
      
        XE.Vendor.GApi.CheckError(cb);      
      });
    },

    DeleteData : function(name, cb) {
    
      var conf = confirm('Are you sure you want to delete ' + name + '?');

      if(conf === false) {
        return false;
      };
  
      this.storage.remove(name, function() {
        XE.Vendor.GApi.CheckError(cb, name);
      });
    },

    CheckError : function(cb, name) {

    if(chrome.runtime.lastError !== undefined) {  

      if(typeof cb.error === 'function') {
        cb.error(); 
      }
      return false;
    }
    else {
      
      if(typeof cb.success === 'function') {
        // Pass name by reference
        
        cb.success(name);
      }
      return true;
    }
    }

  }
}